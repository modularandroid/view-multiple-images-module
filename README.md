# View Multiple Images Module

A configurable and customizable Image Viewer module for android that allows you to display one or multiple images.



## Requirements

You must add the maven repository to your project. To do so, from Android Studio in your **root gradle file, **add the following line: 

```java
allprojects {
     repositories {
		...
        maven { url "https://jitpack.io" }
	}
}
```



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **view-multiple-images-module** directory and click Finish.
     *In case of any import error, just proceed to step 3 and it should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

```java
include ":app", ":view-multiple-images-module"
```
  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

```java
dependencies {
    implementation project(":view-multiple-images-module")
}
```

  5. Click Sync Project with Gradle Files (Sync Now)  

  6. In your **app AndroidManifest.xml**,  under application, add this line : 

```java
<application
	...>
	<activity android:name="com.mecreativestudio.view_multiple_images_module.ViewMultipleImages" />
</application>
```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.view_multiple_images_module.ViewMultipleImages;
```



- If you want to display one or multiple images from their URLs, you can call the module with this simple intent :


```java
Intent intent = new Intent( YOUR_ACTIVITY_NAME.this, ViewMultipleImages.class);
intent.putStringArrayListExtra("imageUrls", YOUR_ARRAYLIST_OF_STRINGS);
startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module. Of course, you must also change YOUR_ARRAYLIST_OF_STRINGS.*



- If you want to display one or multiple images from their paths (associated files), you can call the module with this simple intent :

```java
Intent intent = new Intent( YOUR_ACTIVITY_NAME.this, ViewMultipleImages.class);
intent.putExtra("imageFiles", YOUR_ARRAYLIST_OF_FILES);
startActivity(intent);
```

*In this case, you must change  YOUR_ACTIVITY_NAME and YOUR_ARRAYLIST_OF_FILES.*



### 	Example

Let's say that your application contains an activity, called ***MainActivity***, in which a ***viewMultipleImagesButton*** button is implemented.
You want to display 3 images, from their paths, with the Image Viewer Module when you click on this button. 

By following the previous instructions, the code which you should have to do so is the following : 

```java
import ...
import com.mecreativestudio.view_multiple_images_module.ViewMultipleImages;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Add a click listener on the viewMultipleImagesButton
        Button viewMultipleImagesButton  = findViewById(R.id.viewMultipleImagesButton);
        viewMultipleImagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                //When a click is made on this button, open the viewMultipleImages Module
                Intent intent = new Intent(MainActivity.this, ViewMultipleImages.class);
                
               	//View images from their associated files
                ArrayList<File> imageFiles = new ArrayList<>();
                imageFiles.add(new File("/storage/emulated/0/DCIM/20180816_152522630_341171469f8418737b92af29ce95cc4e.jpg"));
                imageFiles.add(new File("/storage/emulated/0/DCIM/20180816_152530746_ee23e92e54fc93a496795dcf0cfdf70a.jpg"));
                imageFiles.add(new File("/storage/emulated/0/DCIM/20180816_152546598_129851469f841ad95g92af29ce98fs65.jpg"));
                intent.putExtra("imageFiles", imageFiles);
                
                startActivity(intent);
                
            }
        });
    }
}
```

*Note that these paths will not be the same for you, you must therefore adapt them to your case*



## Configuration and Customization

This Image Viewer Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the ViewMultipleImages Java class which is located here :

```java
YOUR_PROJECT/view-multiple-images-module/java.com.mecreativestudio.view_multiple_images_module/ViewMultipleImages.java
```



If you want to adapt its layout to your needs, you can simply edit the view_multiple_images.xml file which is located here :

```java
YOUR_PROJECT/view-multiple-images-module/res/layout/view_multiple_images.xml
```
*Note that this module can also be used as a horizontal scrollview (if you want to display images when creating a post for example). All you have to do is change the height of the viewPager in the file mentioned above.*