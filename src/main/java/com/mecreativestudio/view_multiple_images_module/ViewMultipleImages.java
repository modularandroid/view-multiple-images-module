package com.mecreativestudio.view_multiple_images_module;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;


public class ViewMultipleImages extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_multiple_images);

        ViewPager viewPager = findViewById(R.id.viewPager);

        Intent myIntent = getIntent();
        //If the intent contains both image urls and image paths
        if(myIntent.hasExtra("imageUrls") && myIntent.hasExtra("imageFiles")){
            Log.e("INTENT", "You must choose between image urls or image files");
        }else{

            //If the intent contains only image urls
            if(myIntent.hasExtra("imageUrls")){

                ArrayList<String> imageUrls = myIntent.getExtras().getStringArrayList("imageUrls");
                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(ViewMultipleImages.this, imageUrls, null);
                viewPager.setAdapter(viewPagerAdapter);

            //If the intent contains only image files
            }else if(myIntent.hasExtra("imageFiles")){

                ArrayList<File> imageFiles = (ArrayList<File>)getIntent().getSerializableExtra("imageFiles");
                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(ViewMultipleImages.this, null, imageFiles);
                viewPager.setAdapter(viewPagerAdapter);

            //If the intent contains nothing or something different
            }else{
                Log.e("INTENT", "No or wrong intent has been specified");
            }
        }

    }

}